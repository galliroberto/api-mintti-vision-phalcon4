<?php

namespace Tests\RdP\Domain\ValueObject;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RdP\Domain\ValueObject\Eta;
use TypeError;

final class EtaTest extends TestCase
{
    /**
     * @test
     * @dataProvider parametriValidiProvider
     */
    public function crea_eta_con_parametri_validi($eta): void
    {
        $etaVO = Eta::crea($eta);

        $this->assertEquals($eta, $etaVO->eta());
    }

    public function parametriValidiProvider(): array
    {
        return [
            [12],
            [56],
            [67],
            [70],
        ];
    }

    /**
     * @test
     * @dataProvider parametriNonValidiProvider
     */
    public function crea_eta_con_parametri_non_validi($eta): void
    {
        $this->expectException(InvalidArgumentException::class);

        Eta::crea($eta);
    }

    public function parametriNonValidiProvider(): array
    {
        return [
            [0],
            [13],
            [72],
            [6],
            ['4'],
            [false],
        ];
    }

    /**
     * @test
     * @dataProvider parametriTypeErrorProvider
     */
    public function crea_eta_con_parametri_type_error($eta): void
    {
        $this->expectException(TypeError::class);

        Eta::crea($eta);
    }

    public function parametriTypeErrorProvider(): array
    {
        return [
            [null],
        ];
    }
}
