<?php

namespace Tests\RdP\Domain\ValueObject;

use PHPUnit\Framework\TestCase;
use RdP\Domain\ValueObject\Posizione;
use TypeError;

final class PosizioneTest extends TestCase
{
    /**
     * @test
     * @dataProvider parametriValidiProvider
     */
    public function creaConParametriValidi($lat, $lng): void
    {
        $geo = Posizione::crea($lat, $lng);

        $this->assertEquals($lat, $geo->latitude());
        $this->assertEquals($lng, $geo->longitude());
    }

    public function parametriValidiProvider(): array {
        return [
            [0, 0],
            [10, 10.5],
            ['0', '5'],
            [12.5, 52.7],
        ];
    }

     /**
     * @test
     * @dataProvider parametriTypeErrorProvider
     */
    public function creaConParametriNonValidi($lat, $lng): void {
        $this->expectException(TypeError::class);

        Posizione::crea($lat, $lng);
    }

    public function parametriTypeErrorProvider(): array {
        return [
            [0, null],
            [10, json_encode([5])],
            [true, null],
            [0, 'test'],
        ];
    }
}
