<?php

namespace Tests\RdP\Domain;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Device;

class DeviceTest extends TestCase
{
    /**
     * @test
     */
    public function crea_device(): void
    {
        $info = [];
        $occurredAt = new DateTimeImmutable();

        $device = Device::crea([], $occurredAt);

        $this->assertEquals($info, $device->info());
        $this->assertEquals($occurredAt, $device->occurredAt());
    }
}
