<?php

namespace Tests\RdP\Domain;

use Phalcon\DI;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Rilevamento;
use RdP\Domain\ValueObject\RilevamentoId;
use Tests\Support\Builder\DeviceBuilder;
use Tests\Support\Builder\MisureBuilder;
use Tests\Support\Builder\SoggettoBuilder;

class RilevamentoTest extends TestCase
{
    private $db;

    protected function setUp(): void
    {
        $di = DI::getDefault();
        $this->db = $di->getDb();
        $this->db->begin();
    }

    protected function tearDown(): void
    {
        $this->db->rollback();
    }

    /**
     * @test
     */
    public function crea_rilevamento(): void
    {
        $rilevamentoId = RilevamentoId::crea();

        $rilevamento = Rilevamento::crea($rilevamentoId, DeviceBuilder::crea()->build(), SoggettoBuilder::crea()->build(),
            MisureBuilder::crea()->build());

        $this->assertEquals($rilevamentoId, $rilevamento->id());
    }
}
