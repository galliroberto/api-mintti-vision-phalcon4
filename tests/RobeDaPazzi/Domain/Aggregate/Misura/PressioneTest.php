<?php

namespace Tests\RdP\Domain\Aggegate\Misura;

use DateTimeImmutable;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Misura\Pressione;
use RdP\Domain\ValueObject\MisurazioneParametri;
use TypeError;

final class PressioneTest extends TestCase
{
    /**
     * @test
     * @dataProvider parametriValidiProvider
     */
    public function crea_pressione_con_parametri_validi($sistolica, $diastolica, $battiti): void
    {
        $occurredAt = new DateTimeImmutable();
        $pressione = Pressione::crea($sistolica, $diastolica, $battiti, $occurredAt);

        $this->assertEquals($sistolica, $pressione->sistolica());
        $this->assertEquals($diastolica, $pressione->diastolica());
        $this->assertEquals($battiti, $pressione->battiti());
    }

    public function parametriValidiProvider(): array
    {
        return [
            [120, 80, 50],
            [100, 70, 70],
            [120, 80, 50],
        ];
    }

    /**
     * test
     * @dataProvider parametriNonValidiProvider
     */
    public function crea_pressione_con_parametri_non_validi($sistolica, $diastolica, $battiti): void
    {
        $this->expectException(InvalidArgumentException::class);

        $occurredAt = new DateTimeImmutable();
        Pressione::crea($sistolica, $diastolica, $battiti, $occurredAt);
    }

    public function parametriNonValidiProvider(): array {
        return [
            [150, 250, 300],
            ['l', 120, 't'],
            ['lalal', '1', 'rg'],
            [0, fale, 'f'],
            [false, true, 'bool']
        ];
    }

    /**
     * test
     * @dataProvider parametriTypeErrorProvider
     */
    public function crea_pressione_type_error($sistolica, $diastolica, $battiti): void
    {
        $this->expectException(TypeError::class);

        $occurredAt = new DateTimeImmutable();
        Pressione::crea($sistolica, $diastolica, $battiti, $occurredAt);
    }

    public function parametriTypeErrorProvider(): array {
        return [
            [null, null, null],
        ];
    }
}
