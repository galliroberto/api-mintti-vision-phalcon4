<?php

namespace Tests\Support\Builder;

use DateTimeImmutable;
use RdP\Domain\Aggregate\Misura\Pressione;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\ValueObject\MisurazioneParametri;

final class PressioneBuilder
{
    private int $sistolica;
    private int $diastolica;
    private int $battiti;
    private DateTimeImmutable $occurredAt;

    protected function __construct()
    {
        $this->sistolica = 130;
        $this->diastolica = 130;
        $this->battiti = 130;
        $this->occurredAt = new DateTimeImmutable();
    }

    public static function crea(): self
    {
        return new static();
    }

    public function build(): Temperatura
    {
        return Pressione::crea($this->sistolica, $this->diastolica, $this->battiti, $this->occurredAt);
    }
}