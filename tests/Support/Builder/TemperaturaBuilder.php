<?php

namespace Tests\Support\Builder;

use DateTimeImmutable;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\ValueObject\MisurazioneParametri;

final class TemperaturaBuilder
{
    private float $temperatura;
    private string $unitaMisura;
    private DateTimeImmutable $occurredAt;

    protected function __construct()
    {
        $this->temperatura = 36.8;
        $this->unitaMisura = 'C';
        $this->occurredAt = new DateTimeImmutable();
    }

    public static function crea(): self
    {
        return new static();
    }

    public function build(): Temperatura
    {
        return Temperatura::crea($this->temperatura, $this->unitaMisura, $this->occurredAt);
    }
}