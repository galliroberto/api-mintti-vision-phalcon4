<?php

namespace Tests\Support\Builder;

use RdP\Domain\ValueObject\Eta;

class EtaBuilder
{
    private int $eta;

    protected function __construct()
    {
        $this->eta = Eta::ETA_FASCIA_10_20;
    }

    public static function crea(): self
    {
        return new static();
    }

    public function build(): Eta
    {
        return Eta::crea($this->eta);
    }
}