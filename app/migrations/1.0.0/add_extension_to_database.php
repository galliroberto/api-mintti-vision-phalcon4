<?php

use Phalcon\Migrations\Mvc\Model\Migration;

class AddExtensionToDatabaseMigration_100 extends Migration
{
    public function up()
    {
        $abilitoTimescale = "CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;";
        $this->getConnection()->execute($abilitoTimescale);
        $abilitoPostgis = "CREATE EXTENSION IF NOT EXISTS postgis;";
        $this->getConnection()->execute($abilitoPostgis);
    }

    public function down()
    {
        $abilitoTimescale = "DROP EXTENSION IF EXISTS timescaledb;";
        $this->getConnection()->execute($abilitoTimescale);
        $abilitoPostgis = "DROP EXTENSION IF EXISTS postgis;";
        $this->getConnection()->execute($abilitoPostgis);
    }
}