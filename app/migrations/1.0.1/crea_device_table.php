<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

class CreaDeviceTableMigration_101 extends Migration
{
    public function morph()
    {
        $this->morphTable('device', [
            'columns' => [
                new Column('id', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 36,
                    'notNull' => true,
                    'first' => true,
                ]),
                new Column('info', [
                    'type' => Column::TYPE_JSON,
                    'size' => 3,
                    'after' => 'id',
                ]),
                new Column('occurred_at', [
                    'type' => Column::TYPE_TIMESTAMP,
                    'notNull' => true,
                    'after' => 'longitude',
                ]),
            ],
            'indexes' => [
                new Index('device.PRIMARY', [
                    'id',
                ]),
                new Index('device.occurred_at', [
                    'occurred_at',
                ]),
            ],
        ]);
    }

    public function afterCreateTable()
    {
        /*
         * ALTER TABLE device ADD COLUMN latitude geometry(POINT,2163);
         * ALTER TABLE device ADD COLUMN longitude geometry(POINT,2163);
         *
         * ALTER TABLE table_name
         *  ALTER COLUMN column_name_1 [SET DATA] TYPE new_data_type,
         *  ALTER COLUMN column_name_2 [SET DATA] TYPE new_data_type,
         *  ...;
         */
        $trasformaInHyperTable = "SELECT create_hypertable('device', 'occurred_at');";
        $this->getConnection()->execute($trasformaInHyperTable);
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
    }
}