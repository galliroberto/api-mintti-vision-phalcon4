<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

class CreaSoggettoTableMigration_101 extends Migration
{
    public function morph()
    {
        $this->morphTable('soggetto', [
            'columns' => [
                new Column('id', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 36,
                    'notNull' => true,
                    'first' => true,
                ]),
                new Column('sesso', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 1,
                    'notNull' => true,
                    'after' => 'id',
                ]),
                new Column('eta', [
                    'type' => Column::TYPE_INTEGER,
                    'size' => 3,
                    'notNull' => true,
                    'after' => 'sesso',
                ]),
                new Column('longitude', [
                    'type' => Column::TYPE_DECIMAL,
                    'size' => 20,
                    'scale' => 16,
                    'after' => 'eta',
                ]),
                new Column('latitude', [
                    'type' => Column::TYPE_DECIMAL,
                    'size' => 20,
                    'scale' => 16,
                    'after' => 'longitude',
                ]),
                new Column('occurred_at', [
                    'type' => Column::TYPE_TIMESTAMP,
                    'notNull' => true,
                    'after' => 'latitude',
                ]),
            ],
            'indexes' => [
                new Index('soggetto.PRIMARY', [
                    'id',
                ]),
                new Index('soggetto.sesso', [
                    'sesso',
                ]),
                new Index('soggetto.eta', [
                    'eta',
                ]),
                new Index('soggetto.occurred_at', [
                    'occurred_at',
                ]),
            ],

            /*
            'options' => [
                'TABLE_TYPE'      => 'BASE TABLE',
                'ENGINE'          => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_general_ci',
            ],
            */
        ]);
    }

    public function afterCreateTable()
    {
        /*
         * ALTER TABLE device ADD COLUMN latitude geometry(POINT,2163);
         * ALTER TABLE device ADD COLUMN longitude geometry(POINT,2163);
         *
         * ALTER TABLE table_name
         *  ALTER COLUMN column_name_1 [SET DATA] TYPE new_data_type,
         *  ALTER COLUMN column_name_2 [SET DATA] TYPE new_data_type,
         *  ...;
         */
        $trasformaInHyperTable = "SELECT create_hypertable('soggetto', 'occurred_at');";
        $this->getConnection()->execute($trasformaInHyperTable);

        $posizionePostgis = "ALTER TABLE soggetto ADD COLUMN posizione geometry(POINT,2163);";
        $this->getConnection()->execute($posizionePostgis);
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
    }
}