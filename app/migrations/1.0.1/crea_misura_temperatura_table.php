<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

class CreaMisuraTemperaturaTableMigration_101 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('misura_temperatura', [
            'columns' => [
                new Column('id', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 36,
                    'notNull' => true,
                    'first' => true,
                ]),
                new Column('temperatura', [
                    'type' => Column::TYPE_DECIMAL,
                    'size' => 4,
                    'scale' => 2,
                    'notNull' => false,
                    'after' => 'id',
                ]),
                new Column('unita_misura', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 1,
                    'notNull' => false,
                    'after' => 'temperatura',
                ]),
                new Column('occurred_at', [
                    'type' => Column::TYPE_TIMESTAMP,
                    'notNull' => true,
                    'after' => 'unita_misura',
                ]),
            ],
            'indexes' => [
                new Index('misura_temperatura.PRIMARY', [
                    'id',
                ]),
                new Index('misura_temperatura.occurred_at', [
                    'occurred_at',
                ]),
            ],
        ]);
    }

    public function afterCreateTable()
    {
        $trasformaInHyperTable = "SELECT create_hypertable('misura_temperatura', 'occurred_at');";
        $this->getConnection()->execute($trasformaInHyperTable);
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
    }
}