<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

class CreaMisuraPressioneTableMigration_101 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('misura_pressione', [
            'columns' => [
                new Column('id', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 36,
                    'notNull' => true,
                    'first' => true,
                ]),

                new Column('sistolica', [
                    'type' => Column::TYPE_INTEGER,
                    'size' => 3,
                    'notNull' => false,
                    'after' => 'id',
                ]),
                new Column('diastolica', [
                    'type' => Column::TYPE_INTEGER,
                    'size' => 3,
                    'notNull' => false,
                    'after' => 'id',
                ]),
                new Column('battiti', [
                    'type' => Column::TYPE_INTEGER,
                    'size' => 3,
                    'notNull' => false,
                    'after' => 'diastolica',
                ]),
                new Column('occurred_at', [
                    'type' => Column::TYPE_TIMESTAMP,
                    'notNull' => true,
                    'after' => 'battiti',
                ]),
            ],
            'indexes' => [
                new Index('misura_pressione.PRIMARY', [
                    'id',
                ]),
                new Index('misura_pressione.occurred_at', [
                    'occurred_at',
                ]),
            ],
        ]);
    }

    public function afterCreateTable()
    {
        $trasformaInHyperTable = "SELECT create_hypertable('misura_pressione', 'occurred_at');";
        $this->getConnection()->execute($trasformaInHyperTable);
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
    }
}