<?php

use Phalcon\Config;
use RdP\Application\Util\EnvUtil;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__.'/../../../.env.test');

return new Config([
    'database' => [
        'adapter'     => 'Postgresql',
        'host'        => EnvUtil::get('DB_HOST'),
        'username'    => EnvUtil::get('DB_USER'),
        'password'    => EnvUtil::get('DB_PASS'),
        'dbname'      => EnvUtil::get('DB_NAME'),
        // 'charset'     => 'utf8',
    ],
    'application' => [
        'logInDb' => true,
        'migrationsDir'  => __DIR__ . '/../../migrations/',
    ],
]);

