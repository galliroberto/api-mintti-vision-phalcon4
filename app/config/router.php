<?php

use Phalcon\Events\Event;

$router = $di->getRouter();

// 404
$router->setDefaults(
    [
        'namespace' => '\\RdP\\Infrastructure\\Communication\\Http',
        'controller' => 'NotFound',
        'action' => 'index',
    ]
);

$router->addResource('\\RdP\\Infrastructure\\Communication\\Http\\Rilevamenti', '/api/rilevamenti');

$router->handle($_SERVER['REQUEST_URI']);




