<?php

declare(strict_types=1);

use Phalcon\Db\Dialect\Postgresql as PostgresDialect;
use Phalcon\Db\RawValue;
use Phalcon\Escaper;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Logger;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Model\MetaData\Stream;
use Phalcon\Mvc\Router\Annotations;
use Phalcon\Mvc\View;
use Phalcon\Url as UrlResolver;
use RdP\Domain\Service\CreaRilevamento;
use RdP\Infrastructure\Domain\PhalconRilevamenti;

/**
 * Shared configuration service
 */
$di->setShared(
    'config',
    function () {
        return include APP_PATH . "/config/config.php";
    }
);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared(
    'url',
    function () {
        $config = $this->getConfig();

        $url = new UrlResolver();
        $url->setBaseUri($config->application->baseUri);

        return $url;
    }
);

$di->setShared(
    "router",
    function () {
        // Use the annotations router
        $router = new Annotations(false);

        return $router;
    }
);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */

$di->setShared(
    'db',
    function () {
        $config = $this->getConfig();

        $dialect = new PostgresDialect();

        $dialect->registerCustomFunction(
            'geometry',
            function ($dialect, $expression) {
                $arguments = $expression['arguments'];

                return new RawValue(sprintf(
                    "ST_Transform(ST_SetSRID(ST_MakePoint(%s, %s),4326),2163)",
                    $dialect->getSqlExpression($arguments[0]),
                    $dialect->getSqlExpression($arguments[1])
                ));
            }
        );


        $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
        $params = [
            'host' => $config->database->host,
            'username' => $config->database->username,
            'password' => $config->database->password,
            'dbname' => $config->database->dbname,
            'charset' => $config->database->charset,
            'dialectClass' => $dialect
        ];

        if ($config->database->adapter == 'Postgresql') {
            unset($params['charset']);
        }
        $connection = new $class($params);

        if ($config->database->dbLog) {
            $adapter = new \Phalcon\Logger\Adapter\Stream(sprintf("%s//var/logs/%s_db.log", BASE_PATH, date("Ymd")));
            $logger = new Logger(
                'messages',
                [
                    'db' => $adapter,
                ]
            );

            $eventsManager = new EventsManager();

            $eventsManager->attach(
                'db:beforeQuery',
                function (Event $event, $connection) use ($logger) {
                    $logger->info(":: BEFORE ::");
                    $logger->info($connection->getSQLStatement());
                    $logger->info($connection->getRealSQLStatement());
                }
            );
            $eventsManager->attach(
                'db:afterQuery',
                function (Event $event, $connection) use ($logger) {
                    $logger->info(":: AFTER ::");
                    $logger->info($connection->getSQLStatement());
                    $logger->info($connection->getRealSQLStatement());
                }
            );

            $connection->setEventsManager($eventsManager);
        }

        return $connection;
    }
);

$di->set(
    "modelsManager",
    function () {
        return new ModelsManager();
    }
);

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared(
    'modelsMetadata',

    function () {
        return new MetaDataAdapter();

        /*
         * ToDo
         * NOTE: For local development, the Phalcon\Mvc\Models\MetaData\Memory adapter is recommended so that any changes to the database can be reflected immediately.
         */

        $options = [
            'metaDataDir' => BASE_PATH . '/cache/models_meta',
        ];

        return new Stream($options);
    }
);

$di->set(
    'crea-rilevamento',
    [
        'className' => CreaRilevamento::class,
        'arguments' => [
            [
                'type' => 'parameter',
                'name' => 'rilevamenti',
                'value' => new PhalconRilevamenti(),
            ],
        ]
    ]
);




/**
 * Setting up the view component
 */
/*
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'path' => $config->application->cacheDir,
                'separator' => '_'
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});
*/

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
/*
$di->set('flash', function () {
    $escaper = new Escaper();
    $flash = new Flash($escaper);
    $flash->setImplicitFlush(false);
    $flash->setCssClasses([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);

    return $flash;
});
*/

/**
 * Start the session the first time some component request the session service
 */
/*
$di->setShared('session', function () {
    $session = new SessionManager();
    $files = new SessionAdapter([
        'savePath' => sys_get_temp_dir(),
    ]);
    $session->setAdapter($files);
    $session->start();

    return $session;
});
*/

/*
 * $di->set(
    'Mynamespace\Frontend\Controller\IndexController', array(
    'className' => '\Mynamespace\Frontend\Controller\IndexController',
    'calls' => array(
        array(
            'method' => 'setMySession',
            'arguments' => array(
                array('type' => 'service', 'name' => 'session'),
            )
        ),
    )
));
 */

