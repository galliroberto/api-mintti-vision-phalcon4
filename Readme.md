# LEGGIMI

## per usare docker
per prima cosa andare nella cartella docker e copare il file docker-compose.override.yaml.dist in docker-compose.override.yaml
e al suo interno sono mappate le porte per i servizi
i paramentri di connessione possono non essere sovrascritti e lasciati quelli di default

### per lanciare la docker
`./dc up`

### per entrare nel container
`./dc enter`

### migrazioni db. 
da dentro il container
`./scripts/dev/setup.sh`

### fixture db. 
da dentro il container
`php cli.php fixture run 1000`

### tests
da dentro il container
`vendor/bin/phpunit `



#### Perché timescale
https://assets.timescale.com/whitepapers/20190610_Timescale_WhitePaper_Benchmarking_Influx.pdf

## Timescale
https://docs.timescale.com/latest/tutorials/continuous-aggs-tutorial
https://www.timescale.com/webinar/visualizing-time-series-data-with-timescaledb-and-grafana

-- TIMESTAMPTZ
-- https://docs.timescale.com/latest/api#create_hypertable
- CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
- SELECT create_hypertable('table_name', 'occured_at');

## Postgis
https://postgis.net/docs/postgis_installation.html#create_new_db_extensions
http://live.osgeo.org/archive/11.0/it/quickstart/postgis_quickstart.html#interrogazioni-semplici
https://postgis.net/docs/using_raster_dataman.html#RT_PHP_Output
CREATE EXTENSION postgis;


## Command
### migrations
vendor/bin/phalcon-migrations run --config=app/migrations/env/dev.php
vendor/bin/phalcon-migrations run --config=app/migrations/env/prod.php
vendor/bin/phalcon-migrations run --config=app/migrations/env/test.php --force

### tests
vendor/bin/phpunit 


## ToDo
- creare uno script di setup che inizializza database per timescale, postgis
- registry pattern per le misurazioni
- misura/misure dove una misura può essere una temperatura o una pressione e sono 2 value object diversi (registry pattern)



https://localhost:8009/api/tests
{
    "device": {
        "latitude": 12,
        "longitude": 52
    },
    "soggetto": {
        "sesso": "m",
        "eta": 23
    },
    "misure": {
        "pressione": {
            "sistolica": 130,
            "diastolica": 80,
            "battiti": 57
        },
        "temperatura": {
            "temperatura": 37.5,
            "unita_misura": "C"
        }
    },
    "occurred_at": "2020-05-20 15:00:00"
}
