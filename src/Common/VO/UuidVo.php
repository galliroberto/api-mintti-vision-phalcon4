<?php
namespace Common\VO;


use Ramsey\Uuid\Uuid;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;

class UuidVo
{

    /** @var UuidInterface */
    private $uuid;

    private function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    public static function crea()
    {
        return new static(Uuid::uuid4());
    }

    public static function fromString(string $uuid) : self
    {
        if(!Uuid::isValid($uuid)) {
            throw InvalidArgumentException('uuid non valido');
        }

        return new self(Uuid::fromString($uuid));
    }

    public function toString() : string
    {
        return $this->uuid->toString();
    }

    public static function instance() : self
    {
        return new self(Uuid::uuid4());
    }
}