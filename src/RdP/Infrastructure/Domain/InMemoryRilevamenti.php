<?php

namespace RdP\Infrastructure\Domain;

use RdP\Domain\Aggregate\Rilevamenti;
use RdP\Domain\Aggregate\Rilevamento;
use RdP\Domain\ValueObject\RilevamentoId;

class InMemoryRilevamenti implements Rilevamenti
{
    private $rilevamenti = [];

    public function add(Rilevamento $rilevamento): void
    {
        $this->rilevamenti[$rilevamento->id()->toString()] = $rilevamento;
    }

    public function remove(Rilevamento $rilevamento)
    {
        unset($this->rilevamenti[$rilevamento->id()->toString()]);
    }

    public function findById(RilevamentoId $rilevamentoId)
    {
        if (isset($this->rilevamenti[$rilevamentoId->toString()])) {
            return $this->rilevamenti[$rilevamentoId->toString()];
        }

        return null;
    }

}
