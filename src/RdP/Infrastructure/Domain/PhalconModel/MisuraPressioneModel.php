<?php

namespace RdP\Infrastructure\Domain\PhalconModel;

use Phalcon\Mvc\Model;

class MisuraPressioneModel extends Model
{
    /**
     * @Primary
     * @Identity
     * @Column(type='string', nullable=false)
     */
    private string $id;
    /**
     * @Column(type='integer', nullable=false)
     */
    private float $sistolica;
    /**
     * @Column(type='integer', nullable=false)
     */
    private float $diastolica;
    /**
     * @Column(type='integer', nullable=false)
     */
    private float $battiti;
    /**
     * @Column(type='datetime', nullable=false)
     */
    private string $occurredAt;

    public function initialize()
    {
        $this->setSource('misura_pressione');
    }
}