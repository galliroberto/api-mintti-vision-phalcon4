<?php

namespace RdP\Infrastructure\Domain\PhalconModel;

use Phalcon\Mvc\Model;

class SoggettoModel extends Model
{
    /**
     * @Primary
     * @Identity
     * @Column(type='string', nullable=false)
     */
    private string $id;
    /**
     * @Column(type='string', nullable=false)
     */
    private string $genere;
    /**
     * @Column(type='string', nullable=false)
     */
    private string $eta;
    /**
     * @Column(type='datetime', nullable=false)
     */
    private string $occurredAt;

    public function initialize()
    {
        $this->setSource('soggetto');
    }
}