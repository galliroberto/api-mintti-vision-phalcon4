<?php
namespace RdP\Infrastructure\Domain\PhalconModel;

use Phalcon\Mvc\Model;

class DeviceModel extends Model
{
    /**
     * @Primary
     * @Identity
     * @Column(type='string', nullable=false)
     */
    public string $id;
    /**
     * @Column(type='double', nullable=false)
     */
    public float $lat;
    /**
     * @Column(type='double', nullable=false)
     */
    public float $lng;
    /**
     * @Column(type='string', nullable=true)
     */
    public string $location;
    /**
     * @Column(type='datetime', nullable=false)
     */
    public string $occurredAt;

    public function initialize()
    {
        $this->setSource('device');
    }
}