<?php

namespace RdP\Infrastructure\Domain\PhalconModel;

use Phalcon\Mvc\Model;

class MisuraTemperaturaModel extends Model
{
    /**
     * @Primary
     * @Identity
     * @Column(type='string', nullable=false)
     */
    private string $id;
    /**
     * @Column(type='double', nullable=false)
     */
    private float $temperatura;
    /**
     * @Column(type='double', nullable=true)
     */
    /**
     * @Column(type='datetime', nullable=false)
     */
    private string $occurredAt;

    public function initialize()
    {
        $this->setSource('misura_temperatura');
    }
}