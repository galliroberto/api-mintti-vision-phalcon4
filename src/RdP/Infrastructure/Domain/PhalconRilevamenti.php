<?php

namespace RdP\Infrastructure\Domain;

use Exception;
use Phalcon\Db\RawValue;
use Phalcon\Di;
use RdP\Domain\Aggregate\Device;
use RdP\Domain\Aggregate\Misura\Misure;
use RdP\Domain\Aggregate\Misura\Pressione;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\Aggregate\Rilevamenti;
use RdP\Domain\Aggregate\RilevamentiValidationException;
use RdP\Domain\Aggregate\Rilevamento;
use RdP\Domain\Aggregate\Soggetto;
use RdP\Domain\ValueObject\RilevamentoId;

class PhalconRilevamenti implements Rilevamenti
{
    private $connection;

    public function add(Rilevamento $rilevamento): void
    {
        $this->connection = Di::getDefault()->get('db');
        //$this->startTransaction();
        $deviceResponse = $this->saveDevice($rilevamento->id(), $rilevamento->device());
        $soggettoResponse = $this->saveSoggetto($rilevamento->id(), $rilevamento->soggetto());
        $misuraResponse = $this->saveMisure($rilevamento->id(), $rilevamento->misure());
    }

    private function saveDevice(RilevamentoId $id, Device $device): void
    {
        $sqlPersistenzaDevice = sprintf("INSERT INTO device (id, info, occurred_at) VALUES ('%s', '%s', '%s')",
            $id->toString(),
            json_encode($device->info()),
            $device->occurredAt()->format('Y-m-d H:i:s')
        );

        try {
            $this->connection->execute($sqlPersistenzaDevice);
        } catch (Exception $e) {
            $errors['device'] = ['sql' => str_replace('"', "'", $e->getMessage())];
            throw new RilevamentiValidationException('', 0, $errors);
        }

        return;
    }

    private function saveSoggetto(RilevamentoId $id, Soggetto $soggetto): void
    {
        $sqlPersistenzaSoggetto = sprintf(/** @lang text */ "
            INSERT INTO soggetto 
            (id, sesso, eta, longitude, latitude, posizione, occurred_at) 
            VALUES 
            ('%s', '%s', '%s', %f, %f, ST_Transform(ST_SetSRID(ST_MakePoint(%f, %f),4326),2163), '%s')
            ",
            $id->toString(), $soggetto->sesso()->sesso(), $soggetto->eta()->eta(),
            $soggetto->posizione()->longitude(),
            $soggetto->posizione()->latitude(),
            $soggetto->posizione()->longitude(),
            $soggetto->posizione()->latitude(),
            $soggetto->occurredAt()->format('Y-m-d H:i:s'));

        try {
            $this->connection->execute($sqlPersistenzaSoggetto);
        } catch (Exception $e) {
            $errors['soggetto'] = ['sql' => str_replace('"', "'", $e->getMessage())];
            throw new RilevamentiValidationException('', 0, $errors);
        }
        /*
        $soggettoModel = new SoggettoModel();
        $soggettoModel->assign([
            'id' => $id->toString(),
            'sesso' => $soggetto->sesso()->sesso(),
            'eta' => $soggetto->eta()->eta(),
            'occurred_at' => $occurredAt->format('Y-m-d H:i:s'),
        ]);

        if (true === $soggettoModel->save()) {
            return;
        }

        $messages = json_decode(json_encode($soggettoModel->getMessages()), true);
        $errors['soggeto'] = json_decode(json_encode($messages), true);
        throw new RilevamentiValidationException('', 0, $errors);
        */
    }

    // ToDo
    // iterator pattern!!!!
    // registro in un iteratore tutte le funzioni per il salvatagio ed eseguo la corrispondente ;)

    private function saveMisure(
        RilevamentoId $id,
        Misure $misure
    ): void {
        foreach ($misure->ottieniMisure() as $misura) {
            if ($misura->isTemperatura()) {
                $this->saveTemperatura($id, $misura);
            } else {
                if ($misura->isPressione()) {
                    $this->savePressione($id, $misura);
                }
            }
        }
    }

    private function saveTemperatura(
        RilevamentoId $id,
        Temperatura $temperatura
    ) {
        // ToDo refactoring query
        /*
         $connection->insert(
            "robots",
            ["Astro Boy", 1952],
            ["name", "year"]
        );

        $success = $connection->insertAsDict(
            "robots",
            [
                "name" => "Astro Boy",
                "year" => 1952,
            ]
        );

        $escapedTable = $connection->escapeIdentifier(
            "robots"
        );
         */
        $sqlPersistenzaTemperatura = sprintf(/** @lang text */ "
                INSERT INTO misura_temperatura (id, temperatura, unita_misura, occurred_at) 
                VALUES ('%s', %f, '%s', '%s')
            ",
            $id->toString(),
            $temperatura->temperatura(),
            $temperatura->unitaMisura(),
            $temperatura->occurredAt()->format('Y-m-d H:i:s')
        );

        try {
            $this->connection->execute($sqlPersistenzaTemperatura);
        } catch (Exception $e) {
            $errors['misure']['temperatura'] = ['sql' => str_replace('"', "'", $e->getMessage())];
            throw new RilevamentiValidationException('', 0, $errors);
        }

        return;
        /*
        $misuraTemperaturaModel = new MisuraTemperaturaModel();
        $misuraTemperaturaModel->assign([
            'id' => $id->toString(),
            'temperatura' => $temperatura->temperatura(),
            'unita_misura' => $temperatura->unitaMisura(),
            'occurred_at' => $temperatura->occurredAt()->format('Y-m-d H:i:s'),
        ]);

        if (true === $misuraTemperaturaModel->save()) {
            return;
        }

        $messages = json_decode(json_encode($misuraTemperaturaModel->getMessages()), true);
        $errors['misure']['temperatura'] = json_decode(json_encode($messages), true);
        throw new RilevamentiValidationException('', 0, $errors);
        */
    }

    private function savePressione(
        RilevamentoId $id,
        Pressione $pressione
    ) {
        $sqlPersistenzaPressione = sprintf(/** @lang text */ "
                INSERT INTO misura_pressione (id, sistolica, diastolica, battiti, occurred_at) 
                VALUES ('%s', %d, %d, %d, '%s')
            ",
            $id->toString(),
            $pressione->sistolica(),
            $pressione->diastolica(),
            $pressione->battiti(),
            $pressione->occurredAt()->format('Y-m-d H:i:s')
        );

        try {
            $this->connection->execute($sqlPersistenzaPressione);
        } catch (Exception $e) {
            $errors['misure']['pressione'] = ['sql' => str_replace('"', "'", $e->getMessage())];
            throw new RilevamentiValidationException('', 0, $errors);
        }

        return;
        /*
        $misuraPressioneModel = new MisuraPressioneModel();
        $misuraPressioneModel->assign([
            'id' => $id->toString(),
            'sistolica' => $pressione->sistolica(),
            'diastolica' => $pressione->diastolica(),
            'battiti' => $pressione->battiti(),
            'occurred_at' => $pressione->occurredAt()->format('Y-m-d H:i:s'),
        ]);

        if (true === $misuraPressioneModel->save()) {
            return;
        }

        $messages = json_decode(json_encode($misuraPressioneModel->getMessages()), true);
        $errors['misure']['pressione'] = json_decode(json_encode($messages), true);
        throw new RilevamentiValidationException('', 0, $errors);
        */
    }
}

