<?php

namespace RdP\Infrastructure\Application\Util;

use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;
use Phalcon\Logger\Formatter\Line;
use RdP\Application\Util\LogUtil;

final class LogUtilPhalcon implements LogUtil
{
    private $logger;

    public function __construct()
    {
    }

    public function setFileAdapter(string $streamName)
    {
        $formatter = new Line();
        $formatter->setFormat('[%type%] - [%date%] - %message%');
        $adapter = new Stream(BASE_PATH . '/var/logs/'.$streamName.'.log');

        $adapter->setFormatter($formatter);

        $this->logger = new Logger(
            'messages', [
                $streamName => $adapter,
            ]
        );
    }

    public static function toFile(string $streamName): self {
        $log = new self();
        $log->setFileAdapter($streamName);

        return $log;
    }

    public function alert(string $message): void
    {
        $this->logger->alert($message);
    }

    public function critical(string $message): void
    {
        $this->logger->critical($message);
    }

    public function debug(string $message): void
    {
        $this->logger->debug($message);
    }

    public function error(string $message): void
    {
        $this->logger->error($message);
    }

    public function emergency(string $message): void
    {
        $this->logger->emergency($message);
    }

    public function info(string $message): void
    {
        $this->logger->info($message);
    }

    public function log(string $message): void
    {
        $this->logger->log($message);
    }

    public function notice(string $message): void
    {
        $this->logger->notice($message);
    }

    public function warning(string $message): void
    {
        $this->logger->warning($message);
    }
}









