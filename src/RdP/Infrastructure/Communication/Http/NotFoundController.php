<?php
namespace RdP\Infrastructure\Communication\Http;

use Phalcon\Http\Response;

final class NotFoundController
{
    public function indexAction()
    {
        $response = new Response();
        $response->setStatusCode(404);
        $response->setJsonContent(
            [
                'status' => 'NOT-FOUND'
            ]
        );
        return $response;
    }

}