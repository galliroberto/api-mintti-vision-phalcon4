<?php

namespace RdP\Infrastructure\Communication\Http;

use Exception;
use InvalidArgumentException;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use RdP\Domain\Service\CreaRilevamento;
use RdP\Domain\Service\CreaRilevamentoRequest;

/**
 * @RoutePrefix('/api/rilevamenti')
 */
final class RilevamentiController extends Controller
{
    private CreaRilevamento $creaRilevamento;

    public function onConstruct()
    {
        $this->creaRilevamento = $this->getDi()->get('crea-rilevamento');
    }

    /**
     * @Get(
     *     '/'
     * )
     */
    public function indexAction()
    {
        $response = new Response();
        $response->setJsonContent(
            [
                'status' => 'ok'
            ]
        );

        return $response;
    }

    /**
     * @Post(
     *     '/'
     * )
     */
    public function storeAction()
    {
        $request = new Request();
        if ($request->getContentType() != 'application/json') {
            throw new Exception("Wrong content type");
        }
        $richiestaArray = $request->getJsonRawBody(true);

        $response = new Response();

        try {
            $rilevamentoResponse = $this->creaRilevamento->crea(new CreaRilevamentoRequest($richiestaArray));

            if ($rilevamentoResponse->isSuccess()) {
                $message = [
                    'status' => 'success',
                    'id' => $rilevamentoResponse->message()
                ];
            } else {
                $response->setStatusCode(400);
                $message = [
                    'status' => 'fail',
                    'message' => $rilevamentoResponse->message(),
                    'errors' => $rilevamentoResponse->errors()
                ];
            }
        } catch (InvalidArgumentException $e) {
            $response->setStatusCode(400);
            $message = [
                'status' => 'fail',
                'message' => $e->getMessage(),
                'errors' => explode(PHP_EOL, $e->getTraceAsString())
            ];
        }

        $response->setJsonContent(
            $message
        );

        return $response;
    }
}