<?php

namespace RdP\Domain\Aggregate;

use DateTimeImmutable;
use RdP\Domain\ValueObject\Eta;
use RdP\Domain\ValueObject\Posizione;
use RdP\Domain\ValueObject\Sesso;

final class Soggetto
{
    private Sesso $genere;
    private Eta $eta;
    private Posizione $posizione;
    private DateTimeImmutable $occurredAt;

    private function __construct(
        Sesso $genere,
        Eta $eta,
        Posizione $posizione,
        DateTimeImmutable $occurredAt
    ) {
        $this->genere = $genere;
        $this->eta = $eta;
        $this->posizione = $posizione;
        $this->occurredAt = $occurredAt;
    }

    public static function crea(
        Sesso $genere,
        Eta $eta,
        Posizione $posizione,
        DateTimeImmutable $occurredA
    ): self {
        return new self($genere, $eta, $posizione, $occurredA);
    }

    public function sesso(): Sesso
    {
        return $this->genere;
    }

    public function eta(): Eta
    {
        return $this->eta;
    }

    public function posizione(): Posizione
    {
        return $this->posizione;
    }

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function toArray(): array
    {
        return [
            'sesso' => $this->genere->toArray(),
            'eta' => $this->eta->toArray(),
            'posizione' => $this->posizione->toArray(),
            'occurred_at' => $this->occurredAt->format('Y-m-d H:i:s'),
        ];
    }
}