<?php

namespace RdP\Domain\Aggregate;

use Exception;

class RilevamentiValidationException extends Exception
{
    private $errors;

    public function __construct(
        $message,
        $code = 0,
        $errors = []
    ) {
        parent::__construct($message, $code);

        $this->errors = $errors;
    }

    public function errors(): array
    {
        return $this->errors;
    }
}