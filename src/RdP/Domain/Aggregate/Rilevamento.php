<?php

namespace RdP\Domain\Aggregate;

use RdP\Domain\Aggregate\Misura\Misure;
use RdP\Domain\ValueObject\MisurazioneParametri;
use RdP\Domain\ValueObject\RilevamentoId;

final class Rilevamento
{
    private RilevamentoId $id;
    private Device $device;
    private Soggetto $soggetto;
    private Misure $misure;

    private function __construct(
        RilevamentoId $id,
        Device $device,
        Soggetto $soggetto,
        Misure $misure
    ) {
        $this->id = $id;
        $this->device = $device;
        $this->soggetto = $soggetto;
        $this->misure = $misure;
    }

    public static function crea(
        RilevamentoId $id,
        Device $device,
        Soggetto $soggetto,
        Misure $misure
    ): self {
        return new self($id, $device, $soggetto, $misure);
    }

    public function id(): RilevamentoId
    {
        return $this->id;
    }

    public function device(): Device
    {
        return $this->device;
    }

    public function soggetto(): Soggetto
    {
        return $this->soggetto;
    }

    public function misure(): Misure
    {
        return $this->misure;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id->toString(),
            'device' => $this->device->toArray(),
            'soggetto' => $this->soggetto->toArray(),
            'misure' => $this->misure->toArray(),
        ];
    }
}