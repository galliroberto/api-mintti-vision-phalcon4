<?php

namespace RdP\Domain\Aggregate;

use DateTimeImmutable;

final class Device
{
    private array $info;
    private DateTimeImmutable $occurredAt;

    private function __construct(array $info, DateTimeImmutable $occurredAt)
    {
        $this->info = $info;
        $this->occurredAt = $occurredAt;
    }

    public static function crea(array $info, DateTimeImmutable $occurredAt): self
    {
        return new self($info, $occurredAt);
    }

    public function info(): array
    {
        return $this->info;
    }

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function toArray(): array
    {
        return [
            'info' => $this->info,
            'occurred_at' => $this->occurredAt->format('Y-m-d H:i:s'),
        ];
    }
}