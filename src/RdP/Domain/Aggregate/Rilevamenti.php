<?php

namespace RdP\Domain\Aggregate;

use RdP\Domain\ValueObject\MisurazioneParametri;

interface Rilevamenti
{
    public function add(Rilevamento $rilevamento): void;
}
