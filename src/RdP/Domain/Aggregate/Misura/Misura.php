<?php

namespace RdP\Domain\Aggregate\Misura;

use DateTimeImmutable;

abstract class Misura
{
    public function getNomeMisura(): string
    {
        return static::NOME_MISURA;
    }

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function isTemperatura(): bool
    {
        return Misure::TEMPERATURA == static::NOME_MISURA;
    }

    public function isPressione(): bool
    {
        return Misure::PRESSIONE == static::NOME_MISURA;
    }
}