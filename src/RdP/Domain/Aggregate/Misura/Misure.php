<?php

namespace RdP\Domain\Aggregate\Misura;

use Common\Pattern\Registry;

class Misure
{
    const TEMPERATURA = 'temperatura';
    const PRESSIONE = 'pressione';
    private array $misureConsentite = [
        self::TEMPERATURA,
        self::PRESSIONE,
    ];
    private array $misure = [];

    private function __construct()
    {
    }

    public static function crea(): self
    {
        return new self();
    }

    public function aggiungiMisura(Misura $misura): void
    {
        $this->misure[] = $misura;
    }

    public function ottieniMisure(): array
    {
        return $this->misure;
    }

    public function toArray(): array
    {
        $return = [];
        foreach ($this->misure as $misura) {
            $return[] = $misura->toArray();
        }

        return $return;
    }
}