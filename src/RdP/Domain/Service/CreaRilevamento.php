<?php

namespace RdP\Domain\Service;

use RdP\Domain\Aggregate\Rilevamenti;
use RdP\Domain\Aggregate\RilevamentiValidationException;
use RdP\Domain\Aggregate\Rilevamento;

final class CreaRilevamento
{
    private Rilevamenti $rilevamenti;

    public function __construct(Rilevamenti $rilevamenti)
    {
        $this->rilevamenti = $rilevamenti;
    }

    public function crea(CreaRilevamentoRequest $rilevamentoRequest): CreeRilevamentoResponse
    {
        $rilevamento = Rilevamento::crea(
            $rilevamentoRequest->id(),
            $rilevamentoRequest->device(),
            $rilevamentoRequest->soggetto(),
            $rilevamentoRequest->misure()
        );


        try {
            $this->rilevamenti->add($rilevamento);
        } catch (RilevamentiValidationException $e) {
            return CreeRilevamentoResponse::fail('Salvataggio fallito')->withErrors($e->errors());
        }

        return CreeRilevamentoResponse::success($rilevamentoRequest->id()->toString());
    }
}