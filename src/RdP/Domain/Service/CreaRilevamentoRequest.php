<?php

namespace RdP\Domain\Service;

use DateTimeImmutable;
use InvalidArgumentException;
use RdP\Domain\Aggregate\Device;
use RdP\Domain\Aggregate\Misura\Misure;
use RdP\Domain\Aggregate\Misura\Pressione;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\Aggregate\Soggetto;
use RdP\Domain\ValueObject\Eta;
use RdP\Domain\ValueObject\Posizione;
use RdP\Domain\ValueObject\RilevamentoId;
use RdP\Domain\ValueObject\Sesso;
use Throwable;

class CreaRilevamentoRequest
{
    private RilevamentoId $id;
    private Device $device;
    private Soggetto $soggetto;
    private Misure $misure;

    //private DateTimeImmutable $occurredAt;

    public function __construct(array $request)
    {
        try {
            $this->id = RilevamentoId::crea();
            $this->creaDevice($request['device']);
            $this->creaSoggetto($request['soggetto']);
            $this->creaMisure($request['misure']);
        } catch (Throwable $t) {
            throw new InvalidArgumentException(sprintf("parametri non validi. [%s] ", $t->getMessage()));
        }
    }

    private function creaDevice($device): void
    {
        $info = (array)$device['info'];
        $occurredAt = new DateTimeImmutable($device['occurred_at']);

        $this->device = Device::crea($info, $occurredAt);
    }

    private function creaSoggetto($soggetto): void
    {
        $sesso = Sesso::crea($soggetto['sesso']);
        $eta = Eta::crea($soggetto['eta']);
        $posizione = Posizione::crea($soggetto['posizione']['latitude'], $soggetto['posizione']['longitude']);
        $occurredAt = new DateTimeImmutable($soggetto['occurred_at']);

        $this->soggetto = Soggetto::crea($sesso, $eta, $posizione, $occurredAt);
    }

    private function creaMisure(array $misureRequest): void
    {
        $misure = Misure::crea();

        foreach ($misureRequest as $misura => $parametri) {
            $occurredAt = new DateTimeImmutable($parametri['occurred_at']);
            switch ($misura) {
                case Misure::TEMPERATURA:
                    $misura = Temperatura::crea($parametri['temperatura'], $parametri['unita_misura'], $occurredAt);
                    $misure->aggiungiMisura($misura);
                    break;
                case Misure::PRESSIONE:
                    $misura = Pressione::crea($parametri['sistolica'], $parametri['diastolica'], $parametri['battiti'], $occurredAt);
                    $misure->aggiungiMisura($misura);
                    break;
                default:
                    throw new InvalidArgumentException(sprintf('Misura non valida: %s', $misura));
                    break;
            }
        }

        $this->misure = $misure;
    }

    public function id(): RilevamentoId
    {
        return $this->id;
    }

    public function device(): Device
    {
        return $this->device;
    }

    public function soggetto(): Soggetto
    {
        return $this->soggetto;
    }

    public function misure(): Misure
    {
        return $this->misure;
    }
}