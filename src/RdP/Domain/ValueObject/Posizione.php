<?php
namespace RdP\Domain\ValueObject;

final class Posizione
{
    private float $latitude;
    private float $longiude;

    private function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longiude = $longitude;
    }

    public static function crea(float $latitude, float $longitude): self
    {
        return new self($latitude, $longitude);
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longiude;
    }

    public function toArray(): array {
        return [
            'latitude' => $this->latitude,
            'longitude' => $this->longiude,
        ];
    } 

    public function __toClone():void {}
}