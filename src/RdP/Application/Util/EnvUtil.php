<?php
namespace RdP\Application\Util;

final class EnvUtil
{
    public static function get(string $key, string $default = ''): string
    {
        return getenv($key) ? getenv($key) : ($_ENV[$key] ?? $_ENV[$key] ?? $default);
    }
}