<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Phalcon\Cli\Console;
use Phalcon\Cli\Dispatcher;
use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Loader;
use RdP\Application\Util\EnvUtil;
use Symfony\Component\Dotenv\Dotenv;

define('BASE_PATH', dirname(__DIR__) . '/app');
define('APP_PATH', BASE_PATH . '/app');

$container = new CliDI();
$dispatcher = new Dispatcher();

$dispatcher->setDefaultNamespace('RdP\Infrastructure\Communication\Command');
$dispatcher->setNamespaceName('RdP\Infrastructure\Communication\Command');
$container->setShared('dispatcher', $dispatcher);
$dispatcher->setTaskSuffix('Command');
//$dispatcher->setDefaultTask('fixture');

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__ . '/.env');

$container->setShared(
    'db',
    function () {
        $class = 'Phalcon\Db\Adapter\Pdo\Postgresql';
        $params = [
            'host' => EnvUtil::get('DB_HOST'),
            'username' => EnvUtil::get('DB_USER'),
            'password' => EnvUtil::get('DB_PASS'),
            'dbname' => EnvUtil::get('DB_NAME'),
        ];

        return new $class($params);
    }
);

$console = new Console($container);

$arguments = [];
foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['task'] = $arg;
    } elseif ($k === 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}

try {
    $console->handle($arguments);
} catch (Throwable $throwable) {
    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
    exit(1);
}