<?php

declare(strict_types=1);

use Phalcon\Di\FactoryDefault;
use Phalcon\Events\Event;
use Symfony\Component\Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);
set_error_handler(
    function ($errno, $errstr, $errfile, $errline) {
        throw new \Exception($errstr . PHP_EOL . $errfile . ":" . $errline, $errno);
    }
);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    $di = new FactoryDefault();

    $dotenv = new Dotenv();
    $dotenv->loadEnv(__DIR__ . '/../.env');

    include APP_PATH . '/config/services.php';

    include APP_PATH . '/config/router.php';

    $config = $di->getConfig();

    //include APP_PATH . '/config/loader.php';

    $application = new \Phalcon\Mvc\Application($di);

    echo $application->useImplicitView(false)->handle($_SERVER['REQUEST_URI'])->getContent();
} catch (Throwable $t) {
    http_response_code(500);
    header('Content-Type: application/json');
    echo json_encode(
        [
            'status' => 'fail',
            'message' => explode(PHP_EOL, $t->getMessage()),
            'errors' => explode(PHP_EOL, $t->getTraceAsString())
        ]
    );
}
